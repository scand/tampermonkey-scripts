// ==UserScript==
// @name         GmailDiffAttachmentColorer
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.1.1
// @description  Color diff attached to mail in Gmail
// @author       Alexander Chernyakevich
// @match        https://mail-attachment.googleusercontent.com/*
// @noframes
// @run-at       document-idle
// @grant        GM_log
// ==/UserScript==

(function() {
    'use strict';

    let lines = document.getElementsByTagName('pre')[0].getElementsByTagName('span');
    for (let i=0; i<lines.length; i++) {
        if ( lines[i].innerText.indexOf('<') == 0 ) {
            lines[i].style.color = 'red';
        }
        else if ( lines[i].innerText.indexOf('>') == 0 ) {
            lines[i].style.color = 'green';
        }
    }
})();