// ==UserScript==
// @name         GmailUITweaker
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.1.4
// @description  This script tweaks some UI elements of Gmail (like mail text font size presentation, etc.)
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://mail.google.com/mail/*
// @grant        GM_log
// @grant        GM_registerMenuCommand
// ==/UserScript==

(function() {
    'use strict';

    function tweakStyles() {
        var lastStyleSheet = document.styleSheets[document.styleSheets.length-1];
        lastStyleSheet.insertRule("div.a3s {font-size: medium}", lastStyleSheet.cssRules.length);
        lastStyleSheet.insertRule("div.Am>div {font-size: medium}", lastStyleSheet.cssRules.length);
        lastStyleSheet.insertRule("div.Am {font-size: medium}", lastStyleSheet.cssRules.length);
    }
    setTimeout(tweakStyles, 10000);

    GM_registerMenuCommand("Force tweaking Gmail UI", tweakStyles);
})();
