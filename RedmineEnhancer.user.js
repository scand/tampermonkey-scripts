// ==UserScript==
// @name         RedmineEnhancer
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.2.2
// @description  This script brings enhancement for redmine
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://*/issues/*
// @match        http://*/issues/*
// @grant        GM_log
// @noframes
// ==/UserScript==

(function() {
    'use strict';

    let subtasksDiv = document.getElementById('issue_tree');
    let subtasksHeader = subtasksDiv.getElementsByTagName('P')[0];
    let subtasksTable = subtasksDiv.getElementsByClassName('list issues')[0];

    function getSubtaskStatusInTR(subtasksRow) {
        return subtasksRow.childNodes[2].innerText;
    }
    function toggleSubtasks(targetStatusName, visible) {
        let subtasksRows = subtasksTable.getElementsByTagName('TR');
        for (let i=0; i<subtasksRows.length; i++) {
            if ( subtasksRows[i].className.indexOf('issue issue-') == 0 ) {
                let statusName = getSubtaskStatusInTR(subtasksRows[i]);
                if ( statusName == targetStatusName) {
                    subtasksRows[i].style.display = (visible ? '' : 'none');
                }
            }
        }
    }
    function initSubtasksFilters() {
        let statuses = {};
        let subtasksRows = subtasksTable.getElementsByTagName('TR');
        for (let i=0; i<subtasksRows.length; i++) {
            if ( subtasksRows[i].className.indexOf('issue issue-') == 0 ) {
                let statusName = getSubtaskStatusInTR(subtasksRows[i]);
                statuses[statusName] = ( statuses[statusName] ? statuses[statusName] + 1 : 1);
            }
        }

        for (let status in statuses) {
            let span = document.createElement("span");
            span.innerHTML = '<input type="checkbox" checked="true" id="stStatus' + status + '">' +
                '<label for="stStatus' + status + '">' + status + ' (' + statuses[status] + ')</label>';
            subtasksHeader.appendChild(span);
            let checkbox = document.getElementById("stStatus" + status);
            checkbox.addEventListener('change', function() {
                toggleSubtasks(this.id.substring("stStatus".length), this.checked);
            });
        }
    }

    if (subtasksTable) {
        initSubtasksFilters();
    }

    let relationsDiv = document.getElementById('relations');
    let relationsHeader = relationsDiv.getElementsByTagName('P')[0];
    let relationsTable = relationsDiv.getElementsByClassName('list issues')[0];

    function getRelationStatusInTR(relationsRow) {
        return relationsRow.getElementsByClassName('status')[0].innerText;
    }
    function toggleRelations(targetStatusName, visible) {
        let relationsRows = relationsTable.getElementsByTagName('TR');
        for (let i=0; i<relationsRows.length; i++) {
            let statusName = getRelationStatusInTR(relationsRows[i]);
            if ( statusName == targetStatusName) {
                relationsRows[i].style.display = (visible ? '' : 'none');
            }
        }
    }
    function initRelationsFilters() {
        let statuses = {};
        let relationsRows = relationsTable.getElementsByTagName('TR');
        for (let i=0; i<relationsRows.length; i++) {
            let statusName = getRelationStatusInTR(relationsRows[i]);
            statuses[statusName] = ( statuses[statusName] ? statuses[statusName] + 1 : 1);
        }

        for (let status in statuses) {
            let span = document.createElement("span");
            span.innerHTML = '<input type="checkbox" checked="true" id="rStatus' + status + '">' +
                '<label for="rStatus' + status + '">' + status + ' (' + statuses[status] + ')</label>';
            relationsHeader.appendChild(span);
            let checkbox = document.getElementById("rStatus" + status);
            checkbox.addEventListener('change', function() {
                toggleRelations(this.id.substring("rStatus".length), this.checked);
            });
        }
    }

    if (relationsTable) {
        initRelationsFilters();
    }
})();
