// ==UserScript==
// @name         RSSqlConsole-Helper
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      1.1.5
// @description  Bring additional functionality available from Tampermonkey's menu
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        http://reports.scand/sql-console/console.html
// @grant        GM_registerMenuCommand
// ==/UserScript==

(function() {
    'use strict';

    function getSqlStrTextarea() {
        return document.getElementsByTagName("textarea")[0];
    }
    function setSQLQuery(query) {
        getSqlStrTextarea().value = query;
    }
    function addLeadingZero(value) {
        return ("0" + value).substr(("0" + value).length - 2);
    }
    function findUser() {
        var userNick = prompt("User's nick");
        setSQLQuery("SELECT * FROM tblUsers WHERE UserNick = '" + userNick + "'");
        getSqlStrTextarea().focus();
    }
    function jCatalogLock() {
        var curDate = new Date();
        var lockDate = "" + curDate.getFullYear() +
                       addLeadingZero(curDate.getMonth() + 1) +
                       addLeadingZero(curDate.getDate() - 1);
        lockDate = prompt("Locking date", lockDate);
        setSQLQuery("UPDATE tblProjects SET LockDate=" + lockDate + "\n" +
                    " WHERE LockDate<" + lockDate + "\n" +
                    "   AND ( ProjectName LIKE 'jCatalog%'\n" +
                    "         OR ProjectName LiKE 'Internal (SSD-Java%'\n"+
                    "         OR ProjectName='test introducing')");
        getSqlStrTextarea().focus();
    }
    function jCatalogImportLock() {
        var curDate = new Date();
        var lockDate = "" + curDate.getFullYear() +
                       addLeadingZero(curDate.getMonth() + 1) +
                       addLeadingZero(curDate.getDate());
        lockDate = prompt("Locking date", lockDate);
        setSQLQuery("UPDATE tblProjects SET LockDate=" + lockDate + "\n" +
                    " WHERE ID IN (101, 673)");
        getSqlStrTextarea().focus();
    }
    function moveJCG2Project(targetProject) {
        var curDate = new Date();
        var lockDate = "" + curDate.getFullYear() + addLeadingZero(curDate.getMonth() + 1) + "00";
        lockDate = prompt("Move from date", lockDate);
        setSQLQuery("UPDATE tblReports SET Project=" + targetProject + "\n" +
                    " WHERE ReportDate>" + lockDate + "\n" +
                    "   AND Project=101\n" +
                    "   AND Performer IN (SELECT UserID FROM tblUser2Project WHERE ProjectID=" + targetProject + ")");
        getSqlStrTextarea().focus();
    }
    function moveJCG2JCPROC() {
        moveJCG2Project(1086);
    }
    function moveJCG2JCLAMP() {
        moveJCG2Project(1097);
    }
    function moveJCG2JCERP() {
        moveJCG2Project(1135);
    }
    function totalLock() {
        var curDate = new Date();
        var lockDate = "" + curDate.getFullYear() + addLeadingZero(curDate.getMonth() + 1) + "00";
        lockDate = prompt("Locking date", lockDate);
        if ( lockDate !== null ) {
            setSQLQuery("UPDATE tblProjects SET LockDate=" + lockDate + "\n" +
                        " WHERE LockDate<" + lockDate);
        }
        getSqlStrTextarea().focus();
    }

    GM_registerMenuCommand("Find User", findUser, "u");
    GM_registerMenuCommand("Lock DSS", jCatalogLock, "d");
    GM_registerMenuCommand("Lock jCatalog import", jCatalogImportLock, "i");
    GM_registerMenuCommand("Move JC General to eProc", moveJCG2JCPROC, "p");
    GM_registerMenuCommand("Move JC General to LAMP", moveJCG2JCLAMP, "l");
    GM_registerMenuCommand("Move JC General to ERP", moveJCG2JCERP, "e");
    GM_registerMenuCommand("Lock All Projects", totalLock, "a");

})();
