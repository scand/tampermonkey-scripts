// ==UserScript==
// @name         Autocompleter
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.3.1
// @description  This script brings possibility to automcomplete text by abbreviation followed by Ctrl+SPACE keypress.
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://github.com/*/issues/*
// @match        https://project.opuscapita.com/projectile/start*
// @match        http://reports.scand/addreportuser.php*
// @match        http://reports.scand/addreport.php*
// @match        https://reports.scand.by/addreportuser.php*
// @match        https://reports.scand.by/addreport.php*
// @grant        GM_log
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_registerMenuCommand
// @grant        GM_listValues
// @require      https://github.com/Scandltd/rs-tm-scripts/raw/dev-simplified/common/configHelper.js
// ==/UserScript==
(function() {
    'use strict';

    function expandShortcut(sElement) {
        let val = sElement.value;
        let spaceIdx = val.lastIndexOf(" ");
        let shortcut = val.substring(spaceIdx+1).toLowerCase();
        if ( shortcut.length > 0 && myAutoCompletionList[shortcut] ) {
            sElement.value = val.substr(0, spaceIdx+1) + myAutoCompletionList[shortcut]();
        }
    }

    function customizeAutoCompletionList(list) {
        // sample:
        // {"list" : {"woi" : "Working on internal issues.", "meet" : "Meeting with "}}
        let configStr = GM_getValue('autocompleter.config');
        let config = configHelper.getConfigObject(configStr);
        if ( config != null && config.list != null ) {
            for (let key in config.list) {
                list[key] = () => { return config.list[key] };
                //GM_log(key + " : " + config.list[key]);
            }
        }
    }

    if ( document.location.href.indexOf("https://github.com/") == 0 ) {
        GM_setValue("github.issues.lastLoaded.url", document.location.href);
    } else {
        var myAutoCompletionList = {
            "ana"  : () => "Analyzing.",
            "imp"  : () => "Implementing.",
            "test" : () => "Testing.",
            "doc"  : () => "Documenting.",
            "rev"  : () => "Reviewing.",
            "res"  : () => "Researching.",
            "disc" : () => "Discussing.",
            "ref"  : () => "Refactoring.",
            "rep"  : () => "Reproducing.",
            "fix"  : () => "Fixing.",
            // Handling of GitHub shortcuts
            "@"    : (arg) => {
                var ghIssueUrl = GM_getValue("github.issues.lastLoaded.url", "");

                if ( ghIssueUrl == "" ) {
                    alert("GitHub issues information is not available (probably you didn't open any).");
                    return "@";
                } else {
                    return ghIssueUrl.replace("https://github.com/", "@");
                }
            }
        };

        customizeAutoCompletionList(myAutoCompletionList);

        document.addEventListener('keydown', function(event) {
            let sElement = event.target;
            if ( ( sElement.tagName == "INPUT" || sElement.tagName == "TEXTAREA" ) &&
                event.ctrlKey && ( event.key == '.' || event.key == ' ' ) ) {
                expandShortcut(sElement);
                event.stopPropagation();
                event.preventDefault();
            }
        }, true);
    }

    configHelper.addConfigMenu('autocompleter');
})();
