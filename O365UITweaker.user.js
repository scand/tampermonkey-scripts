// ==UserScript==
// @name         O365UITweaker
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.1.0
// @description  This script tweaks some UI elements of O365 (like header color, etc.)
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://outlook.office.com/*
// @noframes
// @run-at       document-idle
// @grant        GM_log
// ==/UserScript==

(function() {
    'use strict';

    setTimeout(function(){
        var lastStyleSheet = document.styleSheets[document.styleSheets.length-1];
        lastStyleSheet.insertRule("div#O365_NavHeader {background: var(--headerButtonsBackground)}", lastStyleSheet.cssRules.length);
        lastStyleSheet.insertRule(".o365cs-base .o365sx-appName {background: var(--headerButtonsBackground)}", lastStyleSheet.cssRules.length);
        lastStyleSheet.insertRule(".o365cs-base .o365sx-appName:hover, .o365cs-base .o365sx-appName:focus {background: var(--headerButtonsBackgroundHover)}", lastStyleSheet.cssRules.length);
        lastStyleSheet.insertRule(".o365cs-base .o365sx-button {background: var(--headerButtonsBackground)}", lastStyleSheet.cssRules.length);
        lastStyleSheet.insertRule(".o365cs-base .o365sx-button:hover, .o365cs-base .o365sx-button:focus {background: var(--headerButtonsBackgroundHover)}", lastStyleSheet.cssRules.length);
    }, 3000);

})();
