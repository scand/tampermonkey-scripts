// ==UserScript==
// @name         RSReportsConsole-Helper
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      1.1.4
// @description  Bring additional functionality in RS Reports Console available from Tampermonkey's menu
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        http://reports.scand/report_console.php
// @match        http://localhost:10080/report_console.php
// @grant        GM_log
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_registerMenuCommand
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function() {
    'use strict';

    var USER_SERVICE_URL = "http://localhost/reports/users.xml";

    function cleanSelection() {
        var users = document.getElementsByTagName("select")[0];
        for (var j=0; j < users.options.length; j++) {
            users.options[j].selected = false;
        }
    }
    function selectTeam(teamList) {
        var users = document.getElementsByTagName("select")[0];
        for (var i = 0; i < teamList.length; i++) {
            var member = teamList[i];
            var found = false;
            for (var j=0; j < users.options.length; j++) {
                if ( users.options[j].text == member ) {
                    users.options[j].selected = true;
                    found = true;
                    j = users.options.length;
                }
            }
            if ( !found )
                alert("Not found: " + member);
        }
    }
    function selectProjectPeople(isTeamMember) {
        GM_xmlhttpRequest(
            {method: 'GET',
             url: USER_SERVICE_URL + "?" + (new Date().getTime()),
             headers: {'User-agent': 'Mozilla/4.0 (compatible) Greasemonkey',
                       'Accept': 'text/xml',},
             onload: function(response) {
                 //GM_log("ResponseText:" + response.responseText);
                 var teamList = [];

                 var parser = new DOMParser();
                 var xmlDoc = parser.parseFromString(response.responseText, "application/xml");
                 var users = xmlDoc.getElementsByTagName("user");
                 for (var i=0; i < users.length; i++) {
                     var active = ( users[i].getElementsByTagName("active")[0].textContent == "true" );
                     var team = users[i].getElementsByTagName("team")[0].textContent;
                     if ( active && isTeamMember(team) ) {
                         teamList[teamList.length] = "" + users[i].getElementsByTagName("lastname")[0].textContent +
                                                     " " + users[i].getElementsByTagName("firstname")[0].textContent;
                     }
                 }
                 selectTeam(teamList);
                 alert("Total to select: " + teamList.length);
             },
             onerror: function(response) {
                 alert("Error happened: (" + response.readyState + "/" + response.status + ") " + response.statusText);
             }
            });
    }
    function isJCatalogUser(team) {
        return team.indexOf("jc") > -1 || team.indexOf("XSLfast") > -1 || team.indexOf("design") > -1;
    }

    function selectJCatalog() {
        selectProjectPeople(isJCatalogUser);
    }
    function selectCalago() {
        selectProjectPeople( function (team) {
            return team.indexOf("calago") > -1 || team.indexOf("devops") > -1 || team.indexOf("design") > -1;
        } );
    }
    function selectNonJCatalog() {
        selectProjectPeople( function (team) {
            //            return !isJCatalogUser(team);
            return team.indexOf("ojc") > -1;
        } );
    }
    function selectTeamByNamePattern() {
        var teamNamePattern = prompt("Enter search word for Team", GM_getValue("rs.reportsConsole.lastTeamNamePattern", "")).toLowerCase();
        GM_setValue("rs.reportsConsole.lastTeamNamePattern", teamNamePattern);
        selectProjectPeople( function (team) {
            return team.indexOf(teamNamePattern) > -1;
        } );
    }
    function selectAll() {
        selectProjectPeople( function (team) {
            return true;
        } );
    }
    function selectProjects() {
        var pattern = prompt("Enter search word for Projects", GM_getValue("rs.reportsConsole.lastProjectNamePattern", "")).toLowerCase();
        GM_setValue("rs.reportsConsole.lastProjectNamePattern", pattern);
        var projects = document.getElementsByTagName("select")[1];
        let selectedCounter = 0;
        for (var i=0; i < projects.length; i++) {
            var option = projects.options[i];
            if ( option.text.toLowerCase().indexOf(pattern) >= 0 ) {
                option.selected = true;
                selectedCounter++;
            }
        }
        alert("Projects selected: " + selectedCounter);
    }

    GM_registerMenuCommand("Select jCatalog", selectJCatalog);
    GM_registerMenuCommand("Select Calago", selectCalago);
    GM_registerMenuCommand("Select Other than jCatalog", selectNonJCatalog);
    GM_registerMenuCommand("Select Team by Name pattern", selectTeamByNamePattern);
    GM_registerMenuCommand("Select ALL", selectAll);
    GM_registerMenuCommand("Clean selection", cleanSelection);
    GM_registerMenuCommand("Select projects", selectProjects);

})();
