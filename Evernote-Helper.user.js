// ==UserScript==
// @name         Evernote-Helper
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.4
// @description  This helper scripts bring some keyboard automation for web client of Evernote.
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://www.evernote.com/*
// @grant        GM_log
// @grant        GM_registerMenuCommand
// @noframes
// ==/UserScript==

(function() {
    'use strict';

    function toggleLeftPanel() {
        var enlargeButton = document.getElementById('qa-NOTE_FULLSCREEN_BTN');
        var doneButton = document.getElementById('qa-NOTE_FULLSCREEN_BTN');
        if ( enlargeButton && enlargeButton.style.display === "" ) {
            enlargeButton.click();
        } else if ( doneButton && doneButton.style.display === "" ) {
            doneButton.click();
        }
    }

    document.addEventListener('keydown', function(event) {
        //alert(": " + event.ctrlKey +"; " + event.shiftKey + "; "+ event.key);

        // Ctrl+Shift+/ -> Hide Left Panel
        if ( event.ctrlKey && event.shiftKey && ( event.key == '/' || event.key == '|' ) ) {
            toggleLeftPanel();
        }
    }, true);

    GM_registerMenuCommand("Toggle left menu", toggleLeftPanel);
})();
